<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{

    public function index(Request $request)
    {
        $result = $this->retrieveUsers($request);
        return new JsonResponse((Array)$result);
    }

    public function details(Request $request, $login) {
        $result = $this->retrieveUsers($request, $login);
        return new JsonResponse($result);
    }

    public function retrieveUsers($request, $login = null) {

        // retrieve request variable
        $limit = $request->get('limit');
        $offset = $request->get('offset');
        $filter = $request->get('name');
        $limitCount = 0;


        // open file and build array
        $data = file(__DIR__ . "/../../users.csv");
        $fields = explode(',', $data[0]);
        array_shift($data);
        $result = [];



        // loop on array to create user array
        foreach ($data as $i => $line) {

            $datas = explode(",", $line);
            // check if user has the good name
            if ($filter != null) {
                if (!strstr($datas[3], $filter) && !strstr($datas[4], $filter)) {
                    continue;
                }
            }
            // Continue until we do not reach the good offset
            if ($offset != null && $i < $offset) {
                continue;
            }
            // Return one user in login is specified and this user is the user
            if($login != null) {
                if($datas[0] == $login) {
                    $result= [];
                    foreach ($datas as $key => $value) {
                        $result[0][$fields[$key]] = $value;
                    }
                    return $result;
                }
            }
            $array=[];
            foreach ($datas as $key => $value) {
                $array[$fields[$key]] = $value;
            }
            $result[]=$array;
            
            // We stop the loop if we have reach the limit
            $limitCount++;
            if ($limit != null && $limitCount == $limit) {
                break;
            }
        }
        return $result;
    }


}