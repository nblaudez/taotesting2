import {Component } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  protected API_URL  =  'http://localhost:8002';
  protected users;
  protected user;
  protected offset;
  protected filter;
  protected limit;

  constructor(private  httpClient:  HttpClient) {}

  retrieveAll(){

    var query = 0;
    var url = this.API_URL+'/users';
    if(this.offset) {
      url = url + "?offset="+this.offset;
      query = 1;
    }
    if(this.limit) {
      if(query == 1 ) {
        url = url + "&limit="+this.limit;
      } else {
        url = url + "?limit="+this.limit;
      }
      query = 1;
    }
    if(this.filter) {
      if(query == 1 ) {
        url = url + "&name="+this.filter;
      } else {
        url = url + "?name="+this.filter;
      }
    }
    this.httpClient.get(url).subscribe((data:  Array<object>) => {
      this.users  =  data;
    });
  }

  getDetails(userId) {
    return  this.httpClient.get(this.API_URL+'/users/'+userId).subscribe((data:  Array<object>) => {
      this.user  =  data[0];
    });;
  }

}
